// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  app: {
    head: {
      charset: "utf-8",
      viewport: "width=device-width, initial-scale=1",
    },
  },
  modules: ["@pinia/nuxt", "@nuxtjs/i18n"],
  i18n: {
    locales: ["en", "es"], // used in URL path prefix
    defaultLocale: "es", // default locale of your project for Nuxt pages and routings
  },
});
